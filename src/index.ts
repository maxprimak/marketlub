import { initServices } from './serverServices';
import express, { query } from 'express'
import mariadb from 'mariadb'
import dotenv  from 'dotenv'
import cors from 'cors'
import path from 'path' 

dotenv.config({path: '.env'})

const DB_PORT: number = parseInt(process.env.DB_PORT || '3306') 
const PORT: number = parseInt(process.env.PORT || '9090')
const app = express()

const pool = mariadb.createPool({
    host: '127.0.0.1',
    user: process.env.DB_USERNAME || 'root',
    password: process.env.DB_PASSWORD || '12345',
    connectionLimit: 5,
    port: DB_PORT
})
pool.getConnection().then(conn => {
    conn.query('CREATE DATABASE IF NOT EXISTS `market_lub` ').then(() => {
        conn.query('USE `market_lub`')
        conn.query(`SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'`).then(tables => {
            let existsTax = false
            let existsOrders = false
            for(let i = 0; i < tables.length; i++){
                if(tables[i].TABLE_NAME === 'tax'){
                    existsTax = true
                    console.log('Table tax allready exist, skipping creating step...');
                }
                if(tables[i].TABLE_NAME === 'orders'){
                    existsOrders = true 
                    console.log('Table orders allready exist, skipping creating step...');
                }
            }
            if(!existsTax){
                conn.query(`
                    CREATE TABLE tax (
                        id int NOT NULL AUTO_INCREMENT,
                        name varchar(255) NOT NULL,
                        price1 int,
                        price2 int,
                        id_publick VARCHAR(7) NOT NULL UNIQUE,
                        order_id VARCHAR(20),
                        order_action VARCHAR(100),
                        PRIMARY KEY (id)
                    )
                `)
                console.log('tax table created successfully');
                
            }//
            if(!existsOrders){//
                conn.query(`
                    CREATE TABLE orders (
                        id int NOT NULL AUTO_INCREMENT,
                        order_id VARCHAR(20),
                        amount int(11),  
                        action VARCHAR(100),
                        user_id VARCHAR(11),
                        successfull tinyint(1),
                        PRIMARY KEY (id),
                        date datetime DEFAULT(CURDATE())
                    )
                `)
            }//
        }).catch(e => {
            console.warn(e.message);
        })
        
        app.use(cors())
        app.use('/api', initServices(conn) )
        app.use('/', express.static(path.join(__dirname, '../', 'market_client', 'build')))//'../market_client/build'
        
            
    
        app.get('*', (req, res) => {
            res.sendFile(path.resolve(__dirname, '../', 'market_client', 'build', 'index.html'))
        })
    
        app.listen(PORT, () => {
            console.log(`App successfuly started on port ${PORT}!`);
        })
    })
    
})
