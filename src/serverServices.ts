import express from 'express'
import fs from 'fs'
import mariadb from 'mariadb'
import excel from 'exceljs'
import jwt from 'jsonwebtoken'
import md5 from 'md5'
import path from 'path' 
import { LiqPay, LiqPayAPI } from './liqpay'
import axios from 'axios'


export const initServices = (conn: mariadb.PoolConnection) => {
    const app = express.Router()

    const liq = new LiqPay(process.env.LIQPAY_PUBLICK_KEY!, process.env.LIQPAY_PRIVATE_KEY!)

    const auth = (req: express.Request, res: express.Response, next: Function) => {
        const token = req.headers.authorization?.split(' ')[1]
        if(!token){
            res.status(401).json({
                message: ' Unauthorized client error'
            })
        }else{
            try {
                const data = jwt.decode(token)
                //@ts-ignore
                const login: string = data.login
                //@ts-ignore
                const password: string = data.password
                const adminPass = process.env.ADMIN_PASSWORD || 'admin'
                
                if(login === process.env.ADMIN_LOGIN && password === md5(adminPass)){       
                    next()   
                }else{
                    res.status(401).json({
                        message: ' Unauthorized client error'
                    })
                }    
            } catch (error) {
                res.status(401).json({
                    message: ' Unauthorized client error'
                })
            }
            
            
        }
    }
    
    app.get('/set/price/:type', auth,(req,res) => {
        if(req.query.value && typeof req.query.value === 'string' && req.query.id && typeof req.query.id === 'string'){
            const {type} = req.params
            
            if(type === '1' || type === '2'){
                conn.query(`UPDATE tax SET price${type} = ${req.query.value} WHERE id = ${req.query.id}`).then(dbres => {
                    res.status(200).json({
                        message: 'Price seted successfuly'
                    })

                    conn.query(`SELECT id_publick FROM tax WHERE id = ${req.query.id}`).then( dbresult => {
                        conn.query(`INSERT INTO orders ( amount, action, user_id, successfull) VALUES( ${req.query.value}, "set${type}", "${dbresult[0]['id_publick']}", 1   ) `)
                    })
                    
                }).catch(err => {
                    res.status(501).json({
                        message: 'Server issue or bad request!'
                    })
                })  
            }else{
                res.status(400).json({
                    message: 'Bad request!'//
                })
                    
            }
              
            
        }else{
            res.status(400).json({
                message: 'Bad request!'//
            })
        }
    })
    
    app.get('/create', auth, (req, res) => {
        if(req.query.name && typeof req.query.name === 'string'){
            const name: string = req.query.name
            //@ts-ignore
            const id: string = req.query.id
            const price1 = req.query.price1
            const price2 = req.query.price2
            
            conn.query(`INSERT INTO tax (name, price1, price2, id_publick, order_id) VALUES("${name}", ${price1}, ${price2}, "${id}", 0)`).then(dbres => {
                res.status(200).json({
                    message: 'Created successfuly!',
                    id
                })
            }).catch(e => {
                if(e.code == 'ER_DUP_ENTRY'){
                    res.status(402).json({
                        message: 'Name have to be unique!'
                    })
                }else{
                    res.status(501).json({
                        message: 'Server issue!',
                        id: id
                    })
                }
                
            })
    
            
        }else{
            res.status(400).json({
                message: 'Bad request!'
            })
        }
        
        
    })
    app.get('/admin/login', (req, res) => {
        const {login, password} = req.query
        if(login && password){
            const adminPass = process.env.ADMIN_PASSWORD || 'admin'
            if(login === process.env.ADMIN_LOGIN && password === md5(adminPass)){
                res.status(200).json({
                    message: "Logined successfuly",
                    token: jwt.sign({login, password}, process.env.JWT_SECRET || 'standert_jwt_secret')
                })
            }else{
                res.status(406).json({
                    message: 'Invalid login or password'
                })
            }
        }else{
            res.status(400).json({
                message: 'Bad request!'
            })
        }
    })
    app.get('/list/:query', auth, (req, res) => {
        const searchQuery: string = req.params.query
    
        conn.query(`SELECT * FROM tax WHERE id_publick LIKE '%${searchQuery}%' OR name LIKE '%${searchQuery}%' ORDER BY name LIMIT 20`).then(dbres => {
            let result = []
            for(let i = 0; i < dbres.length; i++){
                result.push(dbres[i])
            }
            res.json({data: result})
        })
    })    
    app.get('/list', auth, (req, res) => {
        const offset = req.query.offset || '0'
        conn.query(`SELECT * FROM tax ORDER BY name LIMIT 20 OFFSET ${offset}`).then(dbres => {
            let result = []
            for(let i = 0; i < dbres.length; i++){
                result.push(dbres[i])
            }
            res.json({data: result})
        })
    })
    app.get('/count', auth, (req, res) => {
        conn.query('SELECT COUNT(id) FROM tax').then(dbres => {
            res.json({
                count: dbres[0]["COUNT(id)"]
            })
        })
    })
    app.get('/sum/:type', auth, (req, res) => {
        const type = req.params.type
        if(!!type){
            conn.query(`SELECT SUM(price${type}) FROM tax`).then(dbres => {
                res.json({
                    sum: dbres[0][`SUM(price${type})`]
                })
            })
        }else{
            res.status(400).json({
                message: 'Bad request!'
            })
        }
    })
    app.get('/excel', auth, (req, res) => {
        conn.query('SELECT * FROM tax').then(dbres => {
            const data = []
            for(let i = 0; i < dbres.length; i++){
                data.push(dbres[i])
            }
    
            let workbook = new excel.Workbook()
            let worksheet = workbook.addWorksheet('База')
            worksheet.columns = [
                {
                    header: 'ID',
                    key: "id",
                    width: 10
                },
                {
                    header: "Ім'я",
                    key: 'name',
                    width: 30 
                },
                {
                    header: 'Ціна за оренду',
                    key: "price1",
                    width: 20 
                },
                {
                    header: 'Ціна за електрику',
                    key: "price2",
                    width :20
                },
                {
                    header: 'Публічний ID',
                    key: "id_publick",
                    width :20
                }
            ] 
    
            worksheet.addRows(data)
            const xlsxDump = Date.now()+".xlsx"
            workbook.xlsx.writeFile(path.join(__dirname, xlsxDump)).then(data => {
                res.sendFile(path.join(__dirname, xlsxDump))
            })
    
        })
    
    
    })
    app.get('/submit/:id', async (req, res) => {
        const id_publick = req.params.id.split('_')[0]
        const order_id = req.params.id
        const {data, signature} = liq.formateDataAndSignature({
            version: '3',
            action: 'status',
            order_id 
        })//
        const formData = new URLSearchParams()
        formData.append('data', data) 
        formData.append('signature', signature)
    
        const result= await axios.post('https://www.liqpay.ua/api/request', formData, {headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }})
        
        if(result.data.status === 'success'){
            conn.query(`SELECT order_action, id FROM tax WHERE order_id = "${order_id}"`).then(dbres => {
                if(!dbres[0]){
                    res.status(400).json({
                        message: 'Bad request'
                    }) 
                }else{
                    const action = dbres[0].order_action

                    let successfull = true
                    new Promise( (resolve, reject) => {
                        switch (action){
                            case '1':
                                conn.query(`UPDATE tax SET price1 = price1 - ${result.data.amount}, order_id = "0", order_action = "0"  WHERE order_id = "${order_id}"`).then(resolve).catch(reject)
                            break;
                            case '2':
                                conn.query(`UPDATE tax SET price2 = price2 - ${result.data.amount}, order_id = "0", order_action = "0" WHERE order_id = "${order_id}"`).then(resolve).catch(reject)
                            break;
                            case '1plus2':
                                conn.query(`SELECT price1, price2 FROM tax WHERE order_id = "${order_id}"`).then(dbres => {
                                    const {price1, price2} = dbres[0]
                                    let delay = result.data.amount - price1
                                    if(delay < 0){
                                        conn.query(`UPDATE tax SET price1 = price1 - ${result.data.amount}, order_id = "0", order_action = "0"  WHERE order_id = "${order_id}"`).then(resolve).catch(reject)
                                    }else{
                                        if(delay - price2 < 0){
                                            conn.query(`UPDATE tax SET price2 = price2 - ${delay}, order_id = "0", order_action = "0"  WHERE order_id = "${order_id}"`).then(resolve).catch(reject)
                                        }else{
                                            conn.query(`UPDATE tax SET price1 = 0, price2 = 0, order_id = "0", order_action = "0"  WHERE order_id = "${order_id}"`).then(resolve).catch(reject)
                                        }
                                    }
                                })
                            break;
                        }
                    } ).then(dbres => {
                        res.redirect(`/greating/success?id=${id_publick}`)
                    }).catch(err => {
                        console.log(err);
                        successfull = false
                        res.redirect(`/greating/err?id=${id_publick}`)
                    }).finally(() => {
                        conn.query(`INSERT INTO orders (order_id, amount, action, user_id, successfull) VALUES( "${order_id}", ${result.data.amount}, "${action}", "${id_publick}", ${successfull ? 1 : 0} ) `)
                    })
                    
                }
            }).catch(err => {
                console.log(err);
                res.redirect('/greating/err')
            })
        }else{
            res.redirect('/greating/err')
        }
    })
    app.get('/set/action/:id_publick', (req, res) => {
        if(req.query.order_action && req.params.id_publick){
            conn.query(`UPDATE tax SET order_action = "${req.query.order_action}" WHERE id_publick = ${req.params.id_publick}`).then(dbres => {
                // console.log(`UPDATE tax SET order_action = "${req.query.order_action}" WHERE id_publick = ${req.params.id_publick}`);
                
                res.json({
                    message: 'Success'
                })
            }).catch(err => {
                console.log(err);
                
                res.status(500).json({
                    message: 'Server issue'
                })
            })
        }else{
            res.status(401).json({
                message: "Bad request"
            })
        }
    })

    app.get('/get/orders/:id_publick', (req, res) => {
        const id_publick = req.params.id_publick
        
        if(id_publick && id_publick.length === 5 && parseInt(id_publick)){
            conn.query(`SELECT * FROM orders WHERE user_id = ${id_publick} ORDER BY id DESC LIMIT 20`).then(dbres => {
                res.json({
                    message: 'Orders loaded successfull! Returned 20 last',
                    data: dbres 
                })
            }).catch(err => {
                console.log(err);
                
                res.status(500).json({
                    message: 'Server issue'
                })
            })
        }else{
            res.status(404).json({
                message: 'User not found'
            })
        }
    })
    app.get('/get/news', (req, res) => {
        try {
            const newsText = fs.readFileSync(path.join(__dirname, '../', 'news.txt')).toString('utf-8')
            const splitedData = newsText.split('\n');
            const dateHeader = splitedData.reverse().pop()
            const text = splitedData.reverse().join('\n')
            const date = dateHeader?.split('DATE')[1] || '01.01.2021'

            res.status(200).json({
                text,
                date,
            })
        } catch (error) {
            console.warn(error.message)
            res.status(200).json({
                text: 'Поки немає новин',
                date: '01.01.2021'
            })
        } 
    })
    app.get('/set/news', auth, (req, res) => {
        try {
            const toDay = new Date()
            fs.writeFileSync(path.join(__dirname, '../', 'news.txt'), `DATE ${toDay.getDate()}:${toDay.getMonth()+1}:${toDay.getFullYear()}\n`+req.query.text)    
            res.json({
                message: 'Success'
            })
        } catch (error) {
            res.status(500).json({
                message: 'Can`nt save changes. Server issue'
            })
        }
    })
    app.get('/get/user/:id_publick', (req, res) => {
        const id_publick = req.params.id_publick
        //@ts-ignore
        const password: string = req.query?.password
        if(!password){
            res.status(400).json({
                msg: 'Password is not goted!'
            })
            return
        }
        if(!process.env.USERS_PASSWORD){
            res.status(500).json({
                msg: 'Internal error'
            })
            return
        }

        if(md5(process.env.USERS_PASSWORD) !== password){
            
            res.status(400).json({
                msg: 'Password is incorect!'
            })
            return
        } 
        

        if(id_publick && id_publick.length === 5 && parseInt(id_publick)){
            conn.query(`SELECT order_id FROM tax WHERE id_publick = '${id_publick}'`).then(async (dbres) => {
                console.log(`${process.env.SITENAME}/api/submit/${dbres[0].order_id}`);
                
                axios.get(`${process.env.SITENAME}/api/submit/`+dbres[0].order_id).then( res => {} ).catch(err => {
                    console.log(err, `${process.env.SITENAME}/api/submit/`+dbres[0].order_id);  
                })

                conn.query(`SELECT price1, price2, name FROM tax WHERE id_publick = ${id_publick}`).then((dbres) => {
                    const acc = Math.random()*(10**8)
                    const order_id = Math.floor(acc < 10**8 ? acc + 10**8 : acc)
                    
                    const liqParams: LiqPayAPI = {
                        version: '3',
                        action: 'pay',
                        currency: 'UAH',
                        order_id: id_publick+"_"+order_id,
                        result_url: process.env.SITENAME+"/api/submit/"+id_publick+"_"+order_id
                    }
                    if(!dbres[0]){
                        res.status(404).json(
                            {message: 'User not founded'}
                        )
                    }else{
                        conn.query(`UPDATE tax SET order_id = "${id_publick+"_"+order_id}" WHERE id_publick = '${id_publick}'`)
                        res.status(200).json({
                            message: 'User founded!',
                            price1: dbres[0].price1,
                            price2: dbres[0].price2,
                            name:   dbres[0].name,
                            price1Data: liq.formateDataAndSignature({...liqParams, amount: dbres[0].price1,description: "Оплата оренди в системі базару Любешову." }),
                            price2Data: liq.formateDataAndSignature({...liqParams, amount: dbres[0].price2,description: "Оплата електрики в системі базару Любешову.",}),
                            price1plus2Data: liq.formateDataAndSignature({...liqParams, amount: dbres[0].price1 + dbres[0].price2, description: "Оплата оренди та електрики в системі базару Любешову.",})
                        })
                    }
                    
                })
            })
            
        }else{
            res.status(404).json(
                {message: 'User not founded'}
            )
        }
    })

    return app
}

