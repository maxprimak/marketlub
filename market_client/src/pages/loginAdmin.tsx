import React, { useState } from 'react'
import { useHistory } from 'react-router'
import { DB } from '../DB'

//static
import '../styles/login.css'

export const LoginAdminPage: React.FC = () => {
    const [id, setID] = useState('')
    const [password, setPassword] = useState('')

    const history = useHistory()
    return(
        <main>
            <form className="login_form" style={{height: 450}} onSubmit={e => {
                e.preventDefault()
                DB.loginAdmin(id, password).then(result => {
                    localStorage.setItem('TOKEN', result.token)
                    history.push('/admin')
                })
            }} >
                <h1 className="login-form__title">Вхід у систему ринку Любешів</h1>
                <input type="text" className="login-form__text-input" placeholder="Введіть ID..." onChange={(e: React.FormEvent<HTMLInputElement>) => {
                    const target = e.target as HTMLInputElement
                    const text = target.value
                    setID(text)
                }}/>
                <input type="password" className="login-form__text-input" placeholder="Введіть Пароль..." onChange={(e: React.FormEvent<HTMLInputElement>) => {
                    const target = e.target as HTMLInputElement
                    const text = target.value
                    setPassword(text)
                }}/>
                <button className="btn btn-success" disabled={password.length < 5 || id.length < 5}>Ввійти</button>
            </form>
        </main>
    )
}