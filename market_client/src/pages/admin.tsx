import React, { useEffect, useState } from 'react'

//static
import '../styles/admin.css'
import checkmarck from '../asset/checkmark.svg'
import excelIMG from '../asset/excel.svg'
import newsIMG from '../asset/news.svg'
import { useHistory } from 'react-router'
import { DB } from '../DB'
import { useQuill } from 'react-quilljs'
import 'quill/dist/quill.snow.css'
interface TextContainerProps{
    name: string,
    price1: number
    price2: number,
    id: number,
    id_publick: number
}

const TaxContainer: React.FC<TextContainerProps> = ({name, price1, price2, id, id_publick}) => {
    const [priceFirst, setPriceFirst] = useState(price1.toString())
    const [priceSecond, setPriceSecond] = useState(price2.toString())
    console.log(priceFirst);
    
    const [buttonsDisabled, setButtonsDisabled] = useState(false)

    return(
        <div className="tax-container">
            <div className="tax-container__name">#{id_publick} {name}</div>
            <div className="price__wrapper">
                <div style={{textAlign: "center"}}>
                <div className="prive-label">орендна плата</div>
                <input className="price-little" value={priceFirst} onChange={(e: React.FormEvent<HTMLInputElement>) => {
                    const target = e.target as HTMLInputElement
                    const text = target.value
                    setPriceFirst(text)
                }} />
                </div>
                <button className="btn btn-success small-btn" onClick={e => {
                    setButtonsDisabled(true)
                    DB.setPrice(priceFirst, id,1).then(() => {
                        setButtonsDisabled(false)
                    })
                }} disabled={buttonsDisabled} ><img src={checkmarck} alt="Підтвердження"/></button>
            </div>
            <div className="price__wrapper">
            <div style={{textAlign: "center"}}>
                <div className="prive-label">електрика</div>
                <input className="price-little" value={priceSecond} onChange={(e: React.FormEvent<HTMLInputElement>) => {
                    const target = e.target as HTMLInputElement
                    const text = target.value
                    setPriceSecond(text)
                }}/>
            </div>
                
                <button className="btn btn-success small-btn" onClick={e => {
                    setButtonsDisabled(true)
                    DB.setPrice(priceSecond, id,2).then(() => {
                        setButtonsDisabled(false)
                    })
                }} disabled={buttonsDisabled}><img src={checkmarck} alt="Підтвердження"/></button>
            </div>
        </div>
    )
}

export const AdminPage: React.FC = () => {
    const [pageDisabled, stePageDisabled] = useState(false)
    const [modelVisible, setModelVisible] = useState(false)
    const [quillModalVisible, setQuillModelVisible] = useState(false)
    const [newTax, setNewTax] = useState('')
    const [newTaxName, setNewTaxName] = useState('')
    const [newTaxPrices, setNewTaxPrices] = useState<number[]>([0,0])

    const [taxes, setTaxes] = useState<TextContainerProps[]>([])

    const [price1, setPrice1] = useState(0)
    const [price2, setPrice2] = useState(0)
    
    const {quill, quillRef} = useQuill()
    


    const TOKEN = localStorage.getItem('TOKEN')
    const history = useHistory()
    if(!localStorage.getItem('TOKEN')){
        history.push('/admin/login')
    }

    useEffect(() => {
        DB.getSum(1).then(res => {
            if(res.type === 'success'){
                setPrice1(res.sum)
            }
        })
        DB.getSum(2).then(res => {
            if(res.type === 'success'){
                setPrice2(res.sum)
            }
        })
        if(!modelVisible){
            DB.getCount().then(res => {
                let count = parseInt((parseInt(res.count)/20).toString()) + 2
                let taxesArr: TextContainerProps[] = []
                stePageDisabled(true)
                for(let i = 0; i <= count; i++){
                    DB.getList(i*20).then(res => {
                        taxesArr = [...taxesArr, ...res.data]
                        setTaxes(taxesArr)
                        if(res.data?.length == 0){
                            stePageDisabled(false)
                        }
                    })
                }
            })
        }
        DB.getNews().then(({data}) => {
            quill?.clipboard.dangerouslyPasteHTML(data.text)
        })
        
    }, [modelVisible, quill])
    
    if(!TOKEN){
        history.push('/admin/login')
    }
    return(
        <div className={!pageDisabled ? "admin-wrapper" : "admin-wrapper disabled"} >
            <div className="quill-modal" style={{display: quillModalVisible ? 'block': 'none'}}>
                <button className="btn btn-close-quill" onClick={() => {
                    setQuillModelVisible(false)
                    DB.getNews().then(({data}) => {
                        quill?.clipboard.dangerouslyPasteHTML(data.text)
                    })
                }}>&times;</button>
                <button className="btn btn-save-quill" onClick={() => {
                    setQuillModelVisible(false)
                    DB.setNews(quill?.root.innerHTML)                    
                }}>Зберегти</button>
                <div ref={quillRef}></div>
            </div>
            <div className="create-modal" style={{display: modelVisible ? "flex" : "none"}}>
                <form className="create-modal__body" onSubmit={e => {
                    e.preventDefault()
                    DB.create(newTax, newTaxName, newTaxPrices[0], newTaxPrices[1]).then(res => {
                        if(res.type === 'success'){
                            setModelVisible(false)
                            setNewTax('')
                            setNewTaxName('')
                            setNewTaxPrices([0,0])
                        }else{
                            alert('Неправильний запит')
                        }
                    })
                }}>
                    <button type="button" className="btn btn-close" onClick={e => {setModelVisible(false)}}>&times;</button>
                    <input type="number" className="create-modal__input" placeholder="Введіть код користувача..." onChange={e => {
                        const target = e.target as HTMLInputElement
                        setNewTaxName(target.value)
                    }}/>
                    <input type="text" className="create-modal__input" placeholder="Введіть ПІБ нового користувача..." onChange={e => {
                        const target = e.target as HTMLInputElement
                        setNewTax(target.value)
                    }}/>
                    
                    <div className="create_modal__price-wrapper" style={{display:"flex", justifyContent: 'space-between', width: '60%'}}>
                        <div style={{textAlign: 'center'}}>
                            <p>Ціна за оренду</p>
                            <input type="number" className="create-modal__input" placeholder="Введіть ціну за оренду..." onChange={e => {
                                const target = e.target as HTMLInputElement
                                setNewTaxPrices(prevState => [parseInt(target.value), prevState[1]])
                            }}/>
                        </div>
                        <div style={{textAlign: 'center'}}>
                            <p>Ціна за електрику</p>
                            <input type="number" className="create-modal__input" placeholder="Введіть ціну за електрику..." onChange={e => {
                                const target = e.target as HTMLInputElement
                                setNewTaxPrices(prevState => [prevState[0], parseInt(target.value)])
                            }}/>
                        </div>
                        
                    </div>
                    <button className="btn btn-success" style={{width: "60%"}}>Зберегти</button>
                </form>
            </div>
            <header className="header-admin" style={{marginTop: 0}}>
                <input type="search" placeholder="Введіть пошуковий запит..." className="search-bar" onInput={e => {
                    const target = e.target as HTMLInputElement
                    DB.getListByQuery(target.value).then(res => {
                        if(res.type === 'success'){ setTaxes(res.data) }
                    })
                }}/>               
                <div className="price" data-title="орендна плата">{price1} грн.</div>
                <div className="price" data-title="електрика">{price2} грн.</div>
            </header>
            <div className="tax-wrapper">
                {
                    taxes.map((tax, i) => <TaxContainer {...tax} key={tax.id.toString()}/>)
                }
            </div>
            <footer className="admin-footer">
                <button className="btn btn-news" onClick={e => {setQuillModelVisible(true)}}><img src={newsIMG} alt="news"/></button>
                <button className="btn btn-success btn-add" onClick={e => {setModelVisible(true)}}>Створити новий запис</button>
                <button className="btn btn-exel" onClick={e => {DB.downloadEXCEL()}}><img src={excelIMG} alt="excel"/></button>
            </footer>
        </div>
    )
}