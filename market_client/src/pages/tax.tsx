import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router'
import { useLocation } from 'react-router-dom'
import { AppFooter } from '../component/Footer'
import { AppHeader } from '../component/Header'
import { DB } from '../DB'
import '../styles/tax.css'


const mobileWidth = 540

function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

export const TaxPage: React.FC = () => {
    const [page, setPage] = useState({
        width: window.innerWidth,
        height: window.innerHeight
    })

    const [price1, setPrice1] = useState(0)
    const [price2, setPrice2] = useState(0)
    const [name, setName] = useState('')
    const [data, setData] = useState<{data: string, signature: string}[]>([])
    interface OrderInterface{
        id: number,
        order_id: string,
        amount: string,
        action: string,
        successfull: boolean,
        date: 'string'
    }
     
    const [orders, setOrders] = useState<OrderInterface[]>([])

    const history = useHistory()

    const params = useParams<{id: string}>()
    const query  = useQuery()
    const {id} = params
    
    const toLiq = (target: HTMLButtonElement, data: string, signature: string) => {
        const form = document.createElement('form')
        form.action = 'https://www.liqpay.ua/api/3/checkout'
        form.method = 'POST'
        const inputDATA = document.createElement('input')
        inputDATA.name  = 'data'
        inputDATA.value = data
        form.appendChild(inputDATA)

        const inputSIGNATURE = document.createElement('input')
        inputSIGNATURE.name  = 'signature'
        inputSIGNATURE.value = signature
        form.appendChild(inputSIGNATURE)

        form.classList.add('hidden')
        target.appendChild(form)
        form.submit()
    }

    useEffect(() => {
        DB.loginClient(id, query.get("password") || '').then(res => {
            if(res.type === 'success'){
                setData([res.data.price1Data, res.data.price2Data, res.data.price1plus2Data])
                setName(res.name)                
                setPrice1(res.price1)
                setPrice2(res.price2)
            }else{
                // console.log(id, query.get("password") || '')
                history.push('/login')
            }        
        })
        window.addEventListener('resize', (e) => setPage({
            width: window.innerWidth,
            height: window.innerHeight
        }) ) 
    }, [history, id])
    useEffect(() => {
        DB.getOrderList(id).then(res => {
            if(res.type === 'success'){
                setOrders(res.data)
            }else{
                alert('Не вдалося завантажити історію транзакцій')
            }
        })
    }, [id])

    return(
        <>
        <AppHeader />
        <header>
            <h1 className="main-title">Вітаю <i className="primary">{name}</i></h1>
        </header>
        <section className={page.width < mobileWidth ? "content-wrapper mobile" : 'content-wrapper'}>
            <div className={page.width < mobileWidth ? "tax expanded" : 'tax'}  >
                <h1 className="tax__title">Ваша заборгованість за оренду:</h1>
                <h1 className={price1 === 0 ? "tax__price tax__price-primary" : 'tax__price'}>{price1}грн</h1>
                <button className="btn btn-success pay-btn" disabled={price1 === 0} onClick={async e => {
                    await DB.setPayAction(id, '1')
                    const target = e.target as HTMLButtonElement
                    toLiq(target, data[0].data, data[0].signature)
                }}>Сплатити оренду</button>
            </div>
        
            <div className={page.width < mobileWidth ? "tax expanded" : 'tax'}>
                <h1 className="tax__title">Ваша заборгованість за електрику:</h1>
                <h1 className={price2 === 0 ? "tax__price tax__price-primary" : 'tax__price'}>{price2} грн</h1>
                <button className="btn btn-success pay-btn" disabled={price2 === 0} onClick={async e => {
                    await DB.setPayAction(id, '2')
                    const target = e.target as HTMLButtonElement
                    toLiq(target, data[1].data, data[1].signature)
                }}>Сплатити за електрику</button>
            </div>
        </section>
        <footer>
            <button className={page.width < mobileWidth ? "btn btn-success expanded" : 'btn btn-success'} disabled={(price1 + price2) === 0} onClick={async e => {
                    await DB.setPayAction(id, '1plus2')
                    const target = e.target as HTMLButtonElement
                    toLiq(target, data[2].data, data[2].signature)
                }}>Сплатити усе разом: {price1 + price2} грн</button>
        </footer> 
        <div className="content-wrapper history-wrapper">
            <div className="order_history">
                <ul className="orders">
                    <li className="orders-title">
                        <p className="order-item order-id">#ID</p>
                        <p className="order-item order-amount">Сума</p>
                        <p className="order-item order-action">Операція</p>
                        <p className="order-item order-status">Статус</p>
                        <p className="order-item order-status">Дата</p>
                    </li>
                    {orders.map((order, i) => {
                        let action = 'Оплата'
                        switch(order.action){
                            case '1':
                                action = 'Оплата оренди'
                            break;
                            case '2':
                                action = 'Оплата електрики'
                            break;
                            case '1plus2':
                                action = 'Оплата оренди та електрики'
                            break;
                            case 'set1':
                                action = 'Встановлення ціни за оренду'
                            break;
                            case 'set2':
                                action = 'Встановлення ціни за електрику'
                            break;
                        }
                        return(
                            <li className="order" key={order.id}>
                                <p className="order-item order-id">{order.order_id || '-'}</p>
                                <p className="order-item order-amount">{order.amount}</p>
                                <p className="order-item order-action">{action}</p>
                                <p className="order-item order-status">{order.successfull ? 'Успішно' : 'Невдало'}</p>
                                <p className="order-item order-date">{order.date.slice(0,10).split('-').reduce((acc, i) => acc = i+"."+acc)}</p>
                            </li>
                        )
                    })}
                    
                </ul>
            </div>
        </div>

        <AppFooter />
        </>
    )
}

