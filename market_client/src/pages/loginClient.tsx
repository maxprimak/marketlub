import md5 from 'md5'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import { Link } from 'react-router-dom'
import { AppFooter } from '../component/Footer'
import { AppHeader } from '../component/Header'
import { DB } from '../DB'

//static
import '../styles/login.css'


export const LoginPage: React.FC = () => {
    const [id, setID] = useState('')
    const [password, setPassword] = useState('')
    const [page, setPage] = useState({
        width: window.innerWidth,
        height: window.innerHeight
    })

    const [newsConfig, setNewsConfig] = useState<{
        date: string,
        text: string
    }>({date: '0.0.0000', text: 'Завантажую новини...'})
    const history = useHistory()
    useEffect(() => {
            DB.getNews().then(response => {
                setNewsConfig(response.data)
            })
            window.addEventListener('resize', (e) => setPage({
                width: window.innerWidth,
                height: window.innerHeight
            }) )    
    }, [])
    
    return(
        <>
        <main>
            <AppHeader />
            <div className="news">
                <h1 className="news__title">Новини станом на {newsConfig.date}</h1>
                {/* <p className="news__text">{newsConfig.text}</p> */}
                <div className="news__content" dangerouslySetInnerHTML={{__html: newsConfig.text}}/>
            </div>
            <form className="login_form" onSubmit={async e => {
                e.preventDefault()
                const data = await DB.loginClient(id, md5(password))
                if(data.type === 'error'){
                    alert('Користувача з таким id не найдено або пароль не коректний!')
                }else{
                    history.push(`/${id}?password=${md5(password)}`)    
                }
                                
            }}>
                <h1 className="login-form__title">{page.width < 450 ? "Ринок Любешів" :"Вхід у систему ринку Любешів"}</h1>
                <input type="text" className="login-form__text-input" placeholder="Введіть ID..." onChange={(e: React.FormEvent<HTMLInputElement>) => {
                    const target = e.target as HTMLInputElement
                    const text = target.value
                    setID(text)
                }}/>
                <input type="password" className="login-form__text-input" placeholder="Введіть пароль..." onChange={(e: React.FormEvent<HTMLInputElement>) => {
                    const target = e.target as HTMLInputElement
                    const text = target.value
                    setPassword(text)
                }}/>
                <button className="btn btn-success" disabled={id.length !== 5}>Ввійти</button>
            </form>
        </main>
        <AppFooter />
        </>
    )
}