import React from 'react'
import { useHistory } from 'react-router'
interface AutoRedirectProps{
    path: string
}
export const AutoRedirect: React.FC<AutoRedirectProps> = ({path}) => {
    const history = useHistory()
    history.push(path)
    return(
        <p>Redirection to {path}</p>
    )
}