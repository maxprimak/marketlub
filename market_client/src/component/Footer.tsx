import React from 'react'
import { Link } from 'react-router-dom'


import '../styles/footer.css'
import mastercardLogo from '../asset/mastercard-2.svg'
import visaLogo from '../asset/visa.svg'

//https://www.liqpay.ua/1620311747/static/img/logos/logo-liqpay-main.svg
export const AppFooter: React.FC = () => {
    return (
        <section className="app-footer">
            <div className="links">
                <Link to="about" className="app-footer__link">Про нас</Link><br/>
                <Link to="confidentiality" className="app-footer__link">Умови використання</Link>
            </div>
            <div className="app-footer__logo">
                <img src={mastercardLogo} alt=""/>
            </div>
            <div className="app-footer__logo">
                <img src={visaLogo} alt=""/>
            </div>
            <div className="app-footer__logo">
                <img src="https://www.liqpay.ua/1620311740/static/img/logos/logo-liqpay-white-color.svg" alt=""/>
            </div>
        </section>
    )
}