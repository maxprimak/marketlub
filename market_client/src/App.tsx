import React from 'react';
// import {  } from 'react-router';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { AutoRedirect } from './component/AutoRedirect';
import { GreatingPage } from './pages/greating';
import { LoginAdminPage } from './pages/loginAdmin';
import { AboutPage } from './pages/About';
import { Confidentiality } from './pages/Confidentiality';
import {LoginPage} from './pages/loginClient'
import { TaxPage } from './pages/tax';
import { AdminPage } from './pages/admin';


//<LoginPage />
function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/login">
            <LoginPage />
          </Route>
          <Route path="/admin/login">
            <LoginAdminPage />
          </Route>
          <Route path="/admin">
            <AdminPage />
          </Route>
          <Route path="/greating/:successfull">
            <GreatingPage />
          </Route>
          <Route path="/confidentiality">
            <Confidentiality/>
          </Route>
          <Route path="/about">
            <AboutPage/>
          </Route>
          <Route path="/:id">
            <TaxPage/>
          </Route>
          <Route path="/">
            <AutoRedirect path="/login" />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
