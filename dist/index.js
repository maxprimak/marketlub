"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var serverServices_1 = require("./serverServices");
var express_1 = __importDefault(require("express"));
var mariadb_1 = __importDefault(require("mariadb"));
var dotenv_1 = __importDefault(require("dotenv"));
var cors_1 = __importDefault(require("cors"));
var path_1 = __importDefault(require("path"));
dotenv_1.default.config({ path: '.env' });
var DB_PORT = parseInt(process.env.DB_PORT || '3306');
var PORT = parseInt(process.env.PORT || '9090');
var app = express_1.default();
var pool = mariadb_1.default.createPool({
    host: '127.0.0.1',
    user: process.env.DB_USERNAME || 'root',
    password: process.env.DB_PASSWORD || '12345',
    connectionLimit: 5,
    port: DB_PORT
});
pool.getConnection().then(function (conn) {
    conn.query('CREATE DATABASE IF NOT EXISTS `market_lub` ').then(function () {
        conn.query('USE `market_lub`');
        conn.query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'").then(function (tables) {
            var existsTax = false;
            var existsOrders = false;
            for (var i = 0; i < tables.length; i++) {
                if (tables[i].TABLE_NAME === 'tax') {
                    existsTax = true;
                    console.log('Table tax allready exist, skipping creating step...');
                }
                if (tables[i].TABLE_NAME === 'orders') {
                    existsOrders = true;
                    console.log('Table orders allready exist, skipping creating step...');
                }
            }
            if (!existsTax) {
                conn.query("\n                    CREATE TABLE tax (\n                        id int NOT NULL AUTO_INCREMENT,\n                        name varchar(255) NOT NULL,\n                        price1 int,\n                        price2 int,\n                        id_publick VARCHAR(7) NOT NULL UNIQUE,\n                        order_id VARCHAR(20),\n                        order_action VARCHAR(100),\n                        PRIMARY KEY (id)\n                    )\n                ");
                console.log('tax table created successfully');
            } //
            if (!existsOrders) { //
                conn.query("\n                    CREATE TABLE orders (\n                        id int NOT NULL AUTO_INCREMENT,\n                        order_id VARCHAR(20),\n                        amount int(11),  \n                        action VARCHAR(100),\n                        user_id VARCHAR(11),\n                        successfull tinyint(1),\n                        PRIMARY KEY (id),\n                        date datetime DEFAULT(CURDATE())\n                    )\n                ");
            } //
        }).catch(function (e) {
            console.warn(e.message);
        });
        app.use(cors_1.default());
        app.use('/api', serverServices_1.initServices(conn));
        app.use('/', express_1.default.static(path_1.default.join(__dirname, '../', 'market_client', 'build'))); //'../market_client/build'
        app.get('*', function (req, res) {
            res.sendFile(path_1.default.resolve(__dirname, '../', 'market_client', 'build', 'index.html'));
        });
        app.listen(PORT, function () {
            console.log("App successfuly started on port " + PORT + "!");
        });
    });
});
