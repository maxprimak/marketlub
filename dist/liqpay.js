"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LiqPay = void 0;
var crypto_1 = __importDefault(require("crypto"));
var LiqPay = /** @class */ (function () {
    function LiqPay(public_key, private_key) {
        this.public_key = public_key;
        this.private_key = private_key;
        this.host = 'https://www.liqpay.ua/api/';
    }
    LiqPay.prototype.formateDataAndSignature = function (paramsMinifed) {
        var paramsJSON = JSON.stringify(__assign(__assign({}, paramsMinifed), { public_key: this.public_key }));
        var data = Buffer.from(paramsJSON).toString('base64');
        var sha1 = crypto_1.default.createHash('sha1');
        sha1.update(this.private_key + data + this.private_key);
        var signature = sha1.digest('base64');
        return { data: data, signature: signature };
    };
    LiqPay.prototype.getButtonHTML = function (paramsMinifed) {
        var _a = this.formateDataAndSignature(paramsMinifed), data = _a.data, signature = _a.signature;
        return "\n        <form method=\"POST\" action=\"https://www.liqpay.ua/api/" + paramsMinifed.version + "/checkout\" accept-charset=\"utf-8\">\n         <input type=\"hidden\" name=\"data\" value=\"" + data + "\"/>\n         <input type=\"hidden\" name=\"signature\" value=\"" + signature + "\"/>\n         <input type=\"image\" src=\"https://static.liqpay.ua/buttons/p1ru.radius.png\"/>\n        </form>\n        ";
    };
    return LiqPay;
}());
exports.LiqPay = LiqPay;
//
