"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initServices = void 0;
var express_1 = __importDefault(require("express"));
var fs_1 = __importDefault(require("fs"));
var exceljs_1 = __importDefault(require("exceljs"));
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var md5_1 = __importDefault(require("md5"));
var path_1 = __importDefault(require("path"));
var liqpay_1 = require("./liqpay");
var axios_1 = __importDefault(require("axios"));
var initServices = function (conn) {
    var app = express_1.default.Router();
    var liq = new liqpay_1.LiqPay(process.env.LIQPAY_PUBLICK_KEY, process.env.LIQPAY_PRIVATE_KEY);
    var auth = function (req, res, next) {
        var _a;
        var token = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(' ')[1];
        if (!token) {
            res.status(401).json({
                message: ' Unauthorized client error'
            });
        }
        else {
            try {
                var data = jsonwebtoken_1.default.decode(token);
                //@ts-ignore
                var login = data.login;
                //@ts-ignore
                var password = data.password;
                var adminPass = process.env.ADMIN_PASSWORD || 'admin';
                if (login === process.env.ADMIN_LOGIN && password === md5_1.default(adminPass)) {
                    next();
                }
                else {
                    res.status(401).json({
                        message: ' Unauthorized client error'
                    });
                }
            }
            catch (error) {
                res.status(401).json({
                    message: ' Unauthorized client error'
                });
            }
        }
    };
    app.get('/set/price/:type', auth, function (req, res) {
        if (req.query.value && typeof req.query.value === 'string' && req.query.id && typeof req.query.id === 'string') {
            var type_1 = req.params.type;
            if (type_1 === '1' || type_1 === '2') {
                conn.query("UPDATE tax SET price" + type_1 + " = " + req.query.value + " WHERE id = " + req.query.id).then(function (dbres) {
                    res.status(200).json({
                        message: 'Price seted successfuly'
                    });
                    conn.query("SELECT id_publick FROM tax WHERE id = " + req.query.id).then(function (dbresult) {
                        conn.query("INSERT INTO orders ( amount, action, user_id, successfull) VALUES( " + req.query.value + ", \"set" + type_1 + "\", \"" + dbresult[0]['id_publick'] + "\", 1   ) ");
                    });
                }).catch(function (err) {
                    res.status(501).json({
                        message: 'Server issue or bad request!'
                    });
                });
            }
            else {
                res.status(400).json({
                    message: 'Bad request!' //
                });
            }
        }
        else {
            res.status(400).json({
                message: 'Bad request!' //
            });
        }
    });
    app.get('/create', auth, function (req, res) {
        if (req.query.name && typeof req.query.name === 'string') {
            var name_1 = req.query.name;
            //@ts-ignore
            var id_1 = req.query.id;
            var price1 = req.query.price1;
            var price2 = req.query.price2;
            conn.query("INSERT INTO tax (name, price1, price2, id_publick, order_id) VALUES(\"" + name_1 + "\", " + price1 + ", " + price2 + ", \"" + id_1 + "\", 0)").then(function (dbres) {
                res.status(200).json({
                    message: 'Created successfuly!',
                    id: id_1
                });
            }).catch(function (e) {
                if (e.code == 'ER_DUP_ENTRY') {
                    res.status(402).json({
                        message: 'Name have to be unique!'
                    });
                }
                else {
                    res.status(501).json({
                        message: 'Server issue!',
                        id: id_1
                    });
                }
            });
        }
        else {
            res.status(400).json({
                message: 'Bad request!'
            });
        }
    });
    app.get('/admin/login', function (req, res) {
        var _a = req.query, login = _a.login, password = _a.password;
        if (login && password) {
            var adminPass = process.env.ADMIN_PASSWORD || 'admin';
            if (login === process.env.ADMIN_LOGIN && password === md5_1.default(adminPass)) {
                res.status(200).json({
                    message: "Logined successfuly",
                    token: jsonwebtoken_1.default.sign({ login: login, password: password }, process.env.JWT_SECRET || 'standert_jwt_secret')
                });
            }
            else {
                res.status(406).json({
                    message: 'Invalid login or password'
                });
            }
        }
        else {
            res.status(400).json({
                message: 'Bad request!'
            });
        }
    });
    app.get('/list/:query', auth, function (req, res) {
        var searchQuery = req.params.query;
        conn.query("SELECT * FROM tax WHERE id_publick LIKE '%" + searchQuery + "%' OR name LIKE '%" + searchQuery + "%' ORDER BY name LIMIT 20").then(function (dbres) {
            var result = [];
            for (var i = 0; i < dbres.length; i++) {
                result.push(dbres[i]);
            }
            res.json({ data: result });
        });
    });
    app.get('/list', auth, function (req, res) {
        var offset = req.query.offset || '0';
        conn.query("SELECT * FROM tax ORDER BY name LIMIT 20 OFFSET " + offset).then(function (dbres) {
            var result = [];
            for (var i = 0; i < dbres.length; i++) {
                result.push(dbres[i]);
            }
            res.json({ data: result });
        });
    });
    app.get('/count', auth, function (req, res) {
        conn.query('SELECT COUNT(id) FROM tax').then(function (dbres) {
            res.json({
                count: dbres[0]["COUNT(id)"]
            });
        });
    });
    app.get('/sum/:type', auth, function (req, res) {
        var type = req.params.type;
        if (!!type) {
            conn.query("SELECT SUM(price" + type + ") FROM tax").then(function (dbres) {
                res.json({
                    sum: dbres[0]["SUM(price" + type + ")"]
                });
            });
        }
        else {
            res.status(400).json({
                message: 'Bad request!'
            });
        }
    });
    app.get('/excel', auth, function (req, res) {
        conn.query('SELECT * FROM tax').then(function (dbres) {
            var data = [];
            for (var i = 0; i < dbres.length; i++) {
                data.push(dbres[i]);
            }
            var workbook = new exceljs_1.default.Workbook();
            var worksheet = workbook.addWorksheet('База');
            worksheet.columns = [
                {
                    header: 'ID',
                    key: "id",
                    width: 10
                },
                {
                    header: "Ім'я",
                    key: 'name',
                    width: 30
                },
                {
                    header: 'Ціна за оренду',
                    key: "price1",
                    width: 20
                },
                {
                    header: 'Ціна за електрику',
                    key: "price2",
                    width: 20
                },
                {
                    header: 'Публічний ID',
                    key: "id_publick",
                    width: 20
                }
            ];
            worksheet.addRows(data);
            var xlsxDump = Date.now() + ".xlsx";
            workbook.xlsx.writeFile(path_1.default.join(__dirname, xlsxDump)).then(function (data) {
                res.sendFile(path_1.default.join(__dirname, xlsxDump));
            });
        });
    });
    app.get('/submit/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
        var id_publick, order_id, _a, data, signature, formData, result;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    id_publick = req.params.id.split('_')[0];
                    order_id = req.params.id;
                    _a = liq.formateDataAndSignature({
                        version: '3',
                        action: 'status',
                        order_id: order_id
                    }) //
                    , data = _a.data, signature = _a.signature;
                    formData = new URLSearchParams();
                    formData.append('data', data);
                    formData.append('signature', signature);
                    return [4 /*yield*/, axios_1.default.post('https://www.liqpay.ua/api/request', formData, { headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            } })];
                case 1:
                    result = _b.sent();
                    if (result.data.status === 'success') {
                        conn.query("SELECT order_action, id FROM tax WHERE order_id = \"" + order_id + "\"").then(function (dbres) {
                            if (!dbres[0]) {
                                res.status(400).json({
                                    message: 'Bad request'
                                });
                            }
                            else {
                                var action_1 = dbres[0].order_action;
                                var successfull_1 = true;
                                new Promise(function (resolve, reject) {
                                    switch (action_1) {
                                        case '1':
                                            conn.query("UPDATE tax SET price1 = price1 - " + result.data.amount + ", order_id = \"0\", order_action = \"0\"  WHERE order_id = \"" + order_id + "\"").then(resolve).catch(reject);
                                            break;
                                        case '2':
                                            conn.query("UPDATE tax SET price2 = price2 - " + result.data.amount + ", order_id = \"0\", order_action = \"0\" WHERE order_id = \"" + order_id + "\"").then(resolve).catch(reject);
                                            break;
                                        case '1plus2':
                                            conn.query("SELECT price1, price2 FROM tax WHERE order_id = \"" + order_id + "\"").then(function (dbres) {
                                                var _a = dbres[0], price1 = _a.price1, price2 = _a.price2;
                                                var delay = result.data.amount - price1;
                                                if (delay < 0) {
                                                    conn.query("UPDATE tax SET price1 = price1 - " + result.data.amount + ", order_id = \"0\", order_action = \"0\"  WHERE order_id = \"" + order_id + "\"").then(resolve).catch(reject);
                                                }
                                                else {
                                                    if (delay - price2 < 0) {
                                                        conn.query("UPDATE tax SET price2 = price2 - " + delay + ", order_id = \"0\", order_action = \"0\"  WHERE order_id = \"" + order_id + "\"").then(resolve).catch(reject);
                                                    }
                                                    else {
                                                        conn.query("UPDATE tax SET price1 = 0, price2 = 0, order_id = \"0\", order_action = \"0\"  WHERE order_id = \"" + order_id + "\"").then(resolve).catch(reject);
                                                    }
                                                }
                                            });
                                            break;
                                    }
                                }).then(function (dbres) {
                                    res.redirect("/greating/success?id=" + id_publick);
                                }).catch(function (err) {
                                    console.log(err);
                                    successfull_1 = false;
                                    res.redirect("/greating/err?id=" + id_publick);
                                }).finally(function () {
                                    conn.query("INSERT INTO orders (order_id, amount, action, user_id, successfull) VALUES( \"" + order_id + "\", " + result.data.amount + ", \"" + action_1 + "\", \"" + id_publick + "\", " + (successfull_1 ? 1 : 0) + " ) ");
                                });
                            }
                        }).catch(function (err) {
                            console.log(err);
                            res.redirect('/greating/err');
                        });
                    }
                    else {
                        res.redirect('/greating/err');
                    }
                    return [2 /*return*/];
            }
        });
    }); });
    app.get('/set/action/:id_publick', function (req, res) {
        if (req.query.order_action && req.params.id_publick) {
            conn.query("UPDATE tax SET order_action = \"" + req.query.order_action + "\" WHERE id_publick = " + req.params.id_publick).then(function (dbres) {
                // console.log(`UPDATE tax SET order_action = "${req.query.order_action}" WHERE id_publick = ${req.params.id_publick}`);
                res.json({
                    message: 'Success'
                });
            }).catch(function (err) {
                console.log(err);
                res.status(500).json({
                    message: 'Server issue'
                });
            });
        }
        else {
            res.status(401).json({
                message: "Bad request"
            });
        }
    });
    app.get('/get/orders/:id_publick', function (req, res) {
        var id_publick = req.params.id_publick;
        if (id_publick && id_publick.length === 5 && parseInt(id_publick)) {
            conn.query("SELECT * FROM orders WHERE user_id = " + id_publick + " ORDER BY id DESC LIMIT 20").then(function (dbres) {
                res.json({
                    message: 'Orders loaded successfull! Returned 20 last',
                    data: dbres
                });
            }).catch(function (err) {
                console.log(err);
                res.status(500).json({
                    message: 'Server issue'
                });
            });
        }
        else {
            res.status(404).json({
                message: 'User not found'
            });
        }
    });
    app.get('/get/news', function (req, res) {
        try {
            var newsText = fs_1.default.readFileSync(path_1.default.join(__dirname, '../', 'news.txt')).toString('utf-8');
            var splitedData = newsText.split('\n');
            var dateHeader = splitedData.reverse().pop();
            var text = splitedData.reverse().join('\n');
            var date = (dateHeader === null || dateHeader === void 0 ? void 0 : dateHeader.split('DATE')[1]) || '01.01.2021';
            res.status(200).json({
                text: text,
                date: date,
            });
        }
        catch (error) {
            console.warn(error.message);
            res.status(200).json({
                text: 'Поки немає новин',
                date: '01.01.2021'
            });
        }
    });
    app.get('/set/news', auth, function (req, res) {
        try {
            var toDay = new Date();
            fs_1.default.writeFileSync(path_1.default.join(__dirname, '../', 'news.txt'), "DATE " + toDay.getDate() + ":" + (toDay.getMonth() + 1) + ":" + toDay.getFullYear() + "\n" + req.query.text);
            res.json({
                message: 'Success'
            });
        }
        catch (error) {
            res.status(500).json({
                message: 'Can`nt save changes. Server issue'
            });
        }
    });
    app.get('/get/user/:id_publick', function (req, res) {
        var _a;
        var id_publick = req.params.id_publick;
        //@ts-ignore
        var password = (_a = req.query) === null || _a === void 0 ? void 0 : _a.password;
        if (!password) {
            res.status(400).json({
                msg: 'Password is not goted!'
            });
            return;
        }
        if (!process.env.USERS_PASSWORD) {
            res.status(500).json({
                msg: 'Internal error'
            });
            return;
        }
        if (md5_1.default(process.env.USERS_PASSWORD) !== password) {
            res.status(400).json({
                msg: 'Password is incorect!'
            });
            return;
        }
        if (id_publick && id_publick.length === 5 && parseInt(id_publick)) {
            conn.query("SELECT order_id FROM tax WHERE id_publick = '" + id_publick + "'").then(function (dbres) { return __awaiter(void 0, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    console.log(process.env.SITENAME + "/api/submit/" + dbres[0].order_id);
                    axios_1.default.get(process.env.SITENAME + "/api/submit/" + dbres[0].order_id).then(function (res) { }).catch(function (err) {
                        console.log(err, process.env.SITENAME + "/api/submit/" + dbres[0].order_id);
                    });
                    conn.query("SELECT price1, price2, name FROM tax WHERE id_publick = " + id_publick).then(function (dbres) {
                        var acc = Math.random() * (Math.pow(10, 8));
                        var order_id = Math.floor(acc < Math.pow(10, 8) ? acc + Math.pow(10, 8) : acc);
                        var liqParams = {
                            version: '3',
                            action: 'pay',
                            currency: 'UAH',
                            order_id: id_publick + "_" + order_id,
                            result_url: process.env.SITENAME + "/api/submit/" + id_publick + "_" + order_id
                        };
                        if (!dbres[0]) {
                            res.status(404).json({ message: 'User not founded' });
                        }
                        else {
                            conn.query("UPDATE tax SET order_id = \"" + (id_publick + "_" + order_id) + "\" WHERE id_publick = '" + id_publick + "'");
                            res.status(200).json({
                                message: 'User founded!',
                                price1: dbres[0].price1,
                                price2: dbres[0].price2,
                                name: dbres[0].name,
                                price1Data: liq.formateDataAndSignature(__assign(__assign({}, liqParams), { amount: dbres[0].price1, description: "Оплата оренди в системі базару Любешову." })),
                                price2Data: liq.formateDataAndSignature(__assign(__assign({}, liqParams), { amount: dbres[0].price2, description: "Оплата електрики в системі базару Любешову." })),
                                price1plus2Data: liq.formateDataAndSignature(__assign(__assign({}, liqParams), { amount: dbres[0].price1 + dbres[0].price2, description: "Оплата оренди та електрики в системі базару Любешову." }))
                            });
                        }
                    });
                    return [2 /*return*/];
                });
            }); });
        }
        else {
            res.status(404).json({ message: 'User not founded' });
        }
    });
    return app;
};
exports.initServices = initServices;
